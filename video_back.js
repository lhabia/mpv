import mpvAPI from 'node-mpv'
// where you want to initialise the API

const mpv = new mpvAPI(

);
export async function playMainVideoFirstTime() {
  try{
    await mpv.start();
    await mpv.loadPlaylist('./playlist.txt')
   // await mpv.load('video/main.mp4');
    await mpv.fullscreen();
    //await mpv.loop();

  }
  catch (error) {
    console.log(error);
  }
}
export async function playMainVideo(){

  try{

    await mpv.load('video/main.mp4');
    await mpv.fullscreen();
    await mpv.loop();


  }
  catch (error) {
    console.log(error);
  }
}

export async function playVideo1() {

  try{


      await mpv.jump(1,'replace');
      await mpv.fullscreen();
      await mpv.loop('no');
     // playVideo1 = function(){};
     // await mpv.getPlaylistPosition1 ()


  }
  catch (error) {
    console.log(error);
  }
  //playMainVideo()
}
export async function playVideo2() {
  try{
    await mpv.load('video/2.mp4','replace');
    await mpv.fullscreen();
  }
  catch (error) {
    console.log(error);
  }

}
export async function playVideo3() {
  try{
    await mpv.load('video/3.mp4','replace');
    await mpv.fullscreen();
  }
  catch (error) {
    console.log(error);
  }
}
export async function playVideo4() {
  try{
    await mpv.load('video/4.mp4','replace');
    await mpv.fullscreen();
    await mpv.loop();
  }
  catch (error) {
    console.log(error);
  }
}
