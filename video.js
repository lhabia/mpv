import mpvAPI from 'node-mpv'

const mpv = new mpvAPI({
  "verbose": true,
  "debug": true,
  "auto_restart": true,
  // "audio_only": true
})

export async function initVideo () {
  await mpv.start()
  await mpv.load("video/0.mp4")
  await mpv.fullscreen()
  await mpv.loop("inf")

}

export const playJarVideo = async vid_num => {

  console.log("Start playing JAR video")
  let filename = vid_num + ".mp4"
  let path = "video/" + filename

  let mpv_filename = await mpv.getFilename('stripped')

  try {
    //await mpv.start()
    if (mpv_filename != filename) {
      await mpv.load(path)
      await mpv.fullscreen()
      await mpv.loop('no')
      // await mpv.stop()
      // console.log("Stop playing JAR video")
      await mpv.on('stopped', () => {
        console.log("Start playing Main video")
        mpv.load("video/0.mp4")
        mpv.fullscreen()
        mpv.loop('inf')
      })
      console.log("mpv_filename: " + mpv_filename)
      console.log("video number: " + filename)
    }
  }

  catch (error) {
    console.log(error)
  }

  async function test () {
    console.log("Start playing Main video")
    await mpv.load("video/0.mp4")
    await mpv.fullscreen()
    await mpv.loop('inf')
  }



  console.log("-------------")
}