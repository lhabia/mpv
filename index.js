import { SerialPort } from 'serialport'
import { ReadlineParser } from '@serialport/parser-readline'
import mpvAPI from 'node-mpv'

const JarsIDS = ['671872815', '2432223216', '163303615', '22724620016']
let JarsIDResult = []

let mpv = new mpvAPI({
  //  "verbose": true,
  //  "debug": true,
  "auto_restart": true,
  // "audio_only": true
})

async function init () {

  console.log('MARIO START!!!!!!!!')
  try {
    await mpv.start()
    // await mpv.loadPlaylist('playlist.txt')
    await mpv.load('video/0.mp4')
    await mpv.fullscreen()
    await mpv.loop('inf')

  } catch (error) {
    console.log(error)
  }

}
setInterval(() => {
  mpv.load('video/0.mp4', 'append-play')

}, 3000)

init()

async function playJarVideo (vid_num) {

  console.log("Start playing JAR video")
  let filename = vid_num + ".mp4"
  let path = "video/" + filename

  let mpv_filename = await mpv.getFilename('stripped')

  try {

    if (mpv_filename != filename) {

      await mpv.load(path)
      await mpv.fullscreen()
      await mpv.loop('no')
      console.log('mpv_filename22', mpv_filename)
    }
    console.log("mpv_filename: " + mpv_filename)
    console.log("video number: " + filename)

    mpv.on('stopped', () => {
      console.log("Start playing Main video")
      mpv.load("video/0.mp4")
      mpv.fullscreen()
      mpv.loop('inf')
    })
    // }
  }

  catch (error) {
    console.log(error)
  }
  console.log("-------------")
}

const port = new SerialPort({
  //path: '/dev/cu.usbmodem14101', // mac
  path: '/dev/ttyACM0', //production
  baudRate: 9600,
}).setEncoding('utf8')

const parser = port.pipe(new ReadlineParser({ delimiter: '\r\n' }))

parser.on('data', (data) => {


  JarsIDResult.push(data)

  if (JarsIDResult.length == JarsIDS.length) {

    if (!JarsIDResult.includes('214844761')) {
      playJarVideo("1")
      //  console.log("Jar #1 with ID: " + "2432223216 " + " not in plane")
    }
    if (!JarsIDResult.includes('1021034661')) {

      // console.log("Jar #2 with ID: " + "2432223216 " + " not in plane")
      playJarVideo("2")
    }
    if (!JarsIDResult.includes('2301024661')) {

      // console.log("Jar #3 with ID: " + "163303615 " + " not in plane")
      playJarVideo("3")
    }
    if (!JarsIDResult.includes('8654661')) {

      // console.log("Jar #4 with ID: " + "22724620016 " + " not in plane")
      playJarVideo("4")
    }

    JarsIDResult = []
  }

})
