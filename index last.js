import { SerialPort } from 'serialport'
import { ReadlineParser } from '@serialport/parser-readline'
import mpvAPI from 'node-mpv'

const JarsIDS = ['671872815', '2432223216', '163303615', '22724620016']
let JarsIDResult = []


let mpv = new mpvAPI({
  "verbose": true,
  "debug": true,
  "auto_restart": true,
  // "audio_only": true
},
)


mpv.start()
  .then(() => {
    // This will load and start the song
    return mpv.load('video/0.mp4') && mpv.loop('inf') && mpv.fullscreen()
  })

  // this catches every arror from above
  .catch((error) => {
    console.log(error)
  })

function playJarVideo (a) {
  if (mpv.getFilename('stripped') != '0.mp4') {
    mpv.loop("no")
    mpv.load('video/' + a + '.mp4', 'replace')

    // // mpv.load('video/0.mp4') && mpv.loop('inf') && mpv.fullscreen()
    // mpv.loop("no")
    // mpv.load('video/' + a + '.mp4', 'replace')
    // mpv.fullscreen()
    mpv.on('stopped', () => {
      console.log("Your favorite song just finished, let's start it again!")
      mpv.loadFile('video/1.mp4')
    })
  }
  // .then(() => {

  //   mpv.load('video/0.mp4') && mpv.loop('inf') && mpv.fullscreen()

  // })
}
// // This will bind this function to the stopped event
// mpv.on('stopped', () => {
//   console.log("Your favorite song just finished, let's start it again!")
//   mpv.loadFile('video/1.mp4')
// })



// async function initVideo () {
//   await mpv.start()
//   await mpv.load('video/0.mp4', 'replace')
//   await mpv.loop('inf')
//   await mpv.fullscreen()

// }
// async function mainVideo () {
//   await mpv.fullscreen()
//   await mpv.load("video/0.mp4", "append-play")
//   await mpv.loop("inf")
//   // await mpv.setProperty('fullscreen', true)
// }
// await initVideo()
// async function playJarVideo (a) {
//   if (await mpv.getFilename('stripped') != '1.mp4') {
//     await mpv.loop("no")
//     await mpv.load('video/' + a + '.mp4', 'replace')
//     await mpv.fullscreen()

//     mpv.on('stopped', () => {

//       mainVideo()
//     })
//   }
// }
//
// await mpv.load("video/0.mp4")
// await mpv.fullscreen()
// await mpv.loop("inf")

// const playJarVideo = async vid_num => {

//   console.log("Start playing JAR video")
//   let filename = vid_num + ".mp4"
//   let path = "video/" + filename

//   let mpv_filename = await mpv.getFilename('stripped')

//   try {
//     //await mpv.start()
//     if (mpv_filename != filename) {
//       await mpv.load(path)
//       await mpv.fullscreen()
//       await mpv.loop('no')
//       // await mpv.stop()
//       // console.log("Stop playing JAR video")
//       mpv.on('stopped', () => {
//         console.log("Start playing Main video")
//         mpv.load('video/0.mp4')
//         mpv.fullscreen()
//         mpv.loop('inf')

//       })
//       console.log("mpv_filename: " + mpv_filename)
//       console.log("video number: " + filename)
//     }
//   }

//   catch (error) {
//     console.log(error)
//   }





//   console.log("-------------")
// }


// playJarVideo("0")



const port = new SerialPort({
  path: '/dev/cu.usbmodem14101',
  baudRate: 9600,
}).setEncoding('utf8')

const parser = port.pipe(new ReadlineParser({ delimiter: '\r\n' }))

parser.on('data', (data) => {
  //

  JarsIDResult.push(data)

  if (JarsIDResult.length == JarsIDS.length) {
    // console.log("JarsIDResult.length: " + JarsIDResult.length)
    // console.log("JarsIDS.length: " + JarsIDS.length)
    //console.log(JarsIDResult)

    if (!JarsIDResult.includes('671872815')) {
      playJarVideo(1)

    }
    if (!JarsIDResult.includes('2432223216')) {

      // console.log("Jar #2 with ID: " + "2432223216 " + " not in plane")
      playJarVideo("2")
    }
    if (!JarsIDResult.includes('163303615')) {

      // console.log("Jar #3 with ID: " + "163303615 " + " not in plane")
      playJarVideo("3")
    }
    if (!JarsIDResult.includes('22724620016')) {

      // console.log("Jar #4 with ID: " + "22724620016 " + " not in plane")
      playJarVideo("4")
    }

    /*
    for (let i = 0; i < JarsIDResult.length; i++) {
      checkJARNum(data, JarsIDResult, i)
    } */

    // checkJARNum(data, JarsIDResult)

    JarsIDResult = []

  }

})


// function checkJARNum (id, arr) {

//   console.log("-----------------------")
//   console.log("ID:" + typeof (id) + id)
//   console.log("Arr", typeof (arr) + arr)
//   console.log("-----------------------")

//   if (!arr.includes(id)) {
//     console.log("aaaa")
//     console.log(id)
//     console.log("Jar with ID: " + id + " not in plane")
//   }
// }
