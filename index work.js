import { SerialPort } from 'serialport'
import { ReadlineParser } from '@serialport/parser-readline'
import mpvAPI from 'node-mpv'

const JarsIDS = ['671872815', '2432223216', '163303615', '22724620016']
let JarsIDResult = []


let mpv = new mpvAPI({
  "verbose": true,
  "debug": true,
  "auto_restart": true,
  // "audio_only": true
})
await mpv.start()
await mpv.load('video/0.mp4')
await mpv.fullscreen()
await mpv.loop('inf')
//const playJarVideo = async (vid_num) => {
async function playJarVideo (vid_num) {
  // console.log("Stop playing previous video")
  // mpv.stop()

  console.log("Start playing JAR video")
  let filename = vid_num + ".mp4"
  let path = "video/" + filename

  let mpv_filename = await mpv.getFilename('stripped')

  try {

    if (mpv_filename != filename) {

      await mpv.load(path)
      await mpv.fullscreen()
      await mpv.loop('no')
      console.log('mpv_filename22', mpv_filename)
    }
    console.log("mpv_filename: " + mpv_filename)
    console.log("video number: " + filename)

    // mpv.stop()
    // mpv.load("./video/1.mp4")
    // mpv.loop('no')
    //       mpv.fullscreen()

    mpv.on('stopped', () => {
      console.log("Start playing Main video")
      mpv.load("video/0.mp4")
      mpv.fullscreen()
      mpv.loop('inf')
    })
    // }
  }

  catch (error) {
    console.log(error)
  }
  console.log("-------------")
}
// try {
//   //console.log(path)
//   await mpv.start()
//   await mpv.load('video/0.mp4')
//   // await mpv.fullscreen()
//   // await mpv.loop('inf')
//   // if (mpv_filename != filename) {
//   //   await mpv.load(path)
//   //   await mpv.fullscreen()
//   //   await mpv.loop('no')
//   // await mpv.stop()
//   // console.log("Stop playing JAR video")
//   // mpv.on('stopped', () => {
//   //   console.log("Start playing Main video")
//   //   mpv.load('video/0.mp4')
//   //   mpv.fullscreen()
//   //   mpv.loop('inf')

//   // })
//   // console.log("mpv_filename: " + mpv_filename)
//   // console.log("video number: " + filename)
//   // }
// }

// catch (error) {
//   console.log(error)
// }

// mpv.start()
// let filename = vid_num + ".mp4"
// let path = "video/" + filename

// let mpv_filename = mpv.getFilename('stripped')


//   .then(() => {
//     // This will load and start the song
//     return mpv.load('video/0.mp4') && mpv.loop('inf') && mpv.fullscreen()
//   })

//   // this catches every arror from above
//   .catch((error) => {
//     console.log(error)
//   })

// async function playJarVideo (a) {
//   if (mpv.getFilename('stripped') != '0.mp4') {
//     await mpv.loop("no")
//     await mpv.load('video/' + a + '.mp4', 'replace')

//     // mpv.load('video/0.mp4') && mpv.loop('inf') && mpv.fullscreen()
//     // mpv.loop("no")
//     // mpv.load('video/' + a + '.mp4', 'replace')
//     await mpv.fullscreen()

//   }
//   await mpv.on('stopped', () => {

//     mpv.load('video/0.mp4') && mpv.loop('inf') && mpv.fullscreen()
//   })
// }
// .then(() => {

//   mpv.load('video/0.mp4') && mpv.loop('inf') && mpv.fullscreen()

// })

// // This will bind this function to the stopped event
// mpv.on('stopped', () => {
//   console.log("Your favorite song just finished, let's start it again!")
//   mpv.loadFile('video/1.mp4')
// })



// async function playJarVideo (a) {
//   if (await mpv.getFilename('stripped') != '1.mp4') {
//     await mpv.loop("no")
//     await mpv.load('video/' + a + '.mp4', 'replace')
//     await mpv.fullscreen()

//     mpv.on('stopped', () => {

//       mainVideo()
//     })
//   }
// }
//
// await mpv.load("video/0.mp4")
// await mpv.fullscreen()
// await mpv.loop("inf")

// const playJarVideo = async vid_num => {

//   console.log("Start playing JAR video")
//   let filename = vid_num + ".mp4"
//   let path = "video/" + filename

//   let mpv_filename = await mpv.getFilename('stripped')

//   try {
//     //await mpv.start()
//     if (mpv_filename != filename) {
//       await mpv.load(path)
//       await mpv.fullscreen()
//       await mpv.loop('no')
//       // await mpv.stop()
//       // console.log("Stop playing JAR video")
//       mpv.on('stopped', () => {
//         console.log("Start playing Main video")
//         mpv.load('video/0.mp4')
//         mpv.fullscreen()
//         mpv.loop('inf')

//       })
//       console.log("mpv_filename: " + mpv_filename)
//       console.log("video number: " + filename)
//     }
//   }

//   catch (error) {
//     console.log(error)
//   }





//   console.log("-------------")
// }






const port = new SerialPort({
  path: '/dev/cu.usbmodem14101', // mac
  //path: '/dev/ttyACM0', //production
  baudRate: 9600,
}).setEncoding('utf8')

const parser = port.pipe(new ReadlineParser({ delimiter: '\r\n' }))

parser.on('data', (data) => {


  JarsIDResult.push(data)

  if (JarsIDResult.length == JarsIDS.length) {

    if (!JarsIDResult.includes('671872815')) {
      playJarVideo("1")
      //  console.log("Jar #1 with ID: " + "2432223216 " + " not in plane")
    }
    if (!JarsIDResult.includes('2432223216')) {

      // console.log("Jar #2 with ID: " + "2432223216 " + " not in plane")
      playJarVideo("2")
    }
    if (!JarsIDResult.includes('163303615')) {

      // console.log("Jar #3 with ID: " + "163303615 " + " not in plane")
      playJarVideo("3")
    }
    if (!JarsIDResult.includes('22724620016')) {

      // console.log("Jar #4 with ID: " + "22724620016 " + " not in plane")
      playJarVideo("4")
    }

    JarsIDResult = []

  }

})
